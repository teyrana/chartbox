// GPL v3 (c) 2021, Daniel Williams

#include <algorithm>
#include <chrono>
#include <filesystem>
#include <random>

// may not be standard
#include <sys/stat.h>

#include <cxxopts.hpp>
#include <fmt/format.h>
#include <spdlog/spdlog.h>

// Compile time log levels
#define SPDLOG_ACTIVE_LEVEL SPDLOG_LEVEL_INFO

#include "chart-box/chart-box.hpp"
#include "io/geojson/geojson.hpp"
#include "io/png/png.hpp"

using chartbox::layer::BOUNDARY;
using chartbox::layer::CONTOUR;
using namespace chartbox::io;

static constexpr char program_version[] = "v0.1.0";

int main(int argc, char* argv[]){
    // ====== ====== Define Command Line Arguments ====== ======
    cxxopts::Options options("convert", "Convert chart-related files from one format to another");
    options.add_options("Input", {
        {"b,boundary-path", "Path to the boundary definition file", cxxopts::value<std::string>()->default_value("")},
        {"c,contour-path", "Path to the contour definition file", cxxopts::value<std::string>()->default_value("")},
    });

    options.add_options("Cache", {
        {"cache-path", "Path to the cache directory. To disable caching, set to empty", cxxopts::value<std::string>()->default_value("cache/")},
        // TODO: figure out the value for these:
        // {"rebuild-cache", "Ignore the existing cache, and rebuild any files needed"},
        // {"enable-cache", "enable or disable cache"},
    });

    options.add_options("Output", {
        {"by-layer", "Write out each layer individually (default)"},
        {"by-chart", "Write out contents of all layers, flattened"},
        {"k,kml", "Write output as kml files" },
        {"output-path", "Path to write output files to.", cxxopts::value<std::string>()->default_value("./")},
        {"p,png", "Write output as png files" },
    });

    // ungrouped options
    options.add_options("", {
        {"h,help", "Print usage message and exit."},
        {"v,verbose", "Verbose output"},
        {"version", "Print program version and exit."},
    });

    // /\ /\ /\ /\ /\ /\   Define Configuration      /\ /\ /\ /\ /\ /\.
    // ====== ====== ====== ====== ====== ======    ====== ====== ====== ====== ====== ======    ====== ====== ====== ====== ====== ======
    // \/ \/ \/ \/ \/ \/   Load Configuration        \/ \/ \/ \/ \/ \/

    auto args = options.parse(argc, argv);

    // ====== ====== Process Command Line Arguments ====== ======
    if( args.count("help")) {
        options.set_width(99);
        std::cout << options.help() << std::endl;
        return EXIT_SUCCESS;
    }
    if( args.count("version") ){
        std::cout << program_version << std::endl;
        return EXIT_SUCCESS;
    }

    // create color multi threaded logger
    // auto console = spdlog::stdout_color_mt("console");

    // adjust verbosity of log:
    {   auto verbosity = args.count("verbose");
        if( 1 == verbosity ){
            spdlog::set_level(spdlog::level::info);
        }else if( 2 == verbosity ){
            spdlog::set_level(spdlog::level::debug);
        }else if( 2 < verbosity ){
            spdlog::set_level(spdlog::level::trace);
        }
        spdlog::set_pattern("[%E][%^%l%$] %v");
    }
    spdlog::info( "(info)");
    spdlog::debug("(debug)");
    spdlog::trace("(trace)");

    // ====== ====== Boundary Input Path ====== ======
    const std::filesystem::path boundary_input_path( args["boundary-path"].as<std::string>() );
    if( boundary_input_path.empty() ){
        spdlog::debug( "!! Boundary input path not provided." );
    }else if( ! std::filesystem::is_regular_file(boundary_input_path) ){
        SPDLOG_ERROR(  "!! Boundary input path does not exist !!: {}", boundary_input_path.string() );
        return EXIT_FAILURE;
    }

    // ====== ====== Contour Input Path ====== ======
    const std::filesystem::path contour_input_path( args["contour-path"].as<std::string>() );
    if( contour_input_path.empty() ){
        spdlog::debug( "!! Contour input path not provided." );
    }else if( ! std::filesystem::is_regular_file(contour_input_path)){
        SPDLOG_ERROR(  "!! Contour input path does not exist !!: ", contour_input_path.string() );
        return EXIT_FAILURE;
    }

    // ====== ====== Cache Path ====== ======
    const std::filesystem::path cache_path( args["cache-path"].as<std::string>() );
    const bool cache_enable = (cache_path.empty()?false:true);
    if( cache_enable and not std::filesystem::is_directory(cache_path) ){
        if( std::filesystem::create_directory(cache_path) ){
            SPDLOG_INFO( ":: Info: creating cache path: {}", cache_path.string() );
        }else{
            SPDLOG_ERROR( "!! Cache path could not be created!: {}", cache_path.string() );
            return EXIT_FAILURE;
        }
    }else{
        SPDLOG_INFO( "Cache Path is empty--disabling cache" );
    }
    // const bool cache_flush = args["flush-cache"].as<bool>();

    // ====== ====== Output Arguments ====== ======
    const bool output_each_layer = args["by-layer"].as<bool>();
    const bool output_chart = args["by-chart"].as<bool>() or not output_each_layer;
    const std::filesystem::path output_path{ args["output-path"].as<std::string>() };
    const bool output_as_kml = args["kml"].as<bool>();
    const bool output_as_png = args["png"].as<bool>();

    // if( not output_each_layer and not output_chart ){
    //     output_chart = true;
    // }

    // Debug: Print Arguments:
    // ==============
    SPDLOG_INFO( "    ====== Input: ======" );
    SPDLOG_INFO( "        ::Boundary:   {}", boundary_input_path.string() );
    SPDLOG_INFO( "        ::Contour:    {}", contour_input_path.string() );
    SPDLOG_INFO( "    ====== Cache: ======" );
    SPDLOG_INFO( "        ::Enable:     {}", (cache_enable?"Enabled":"Disabled") );
    SPDLOG_INFO( "        ::Path:       {}", cache_path.string() );
    SPDLOG_INFO( "    ====== Output: ======" );
    SPDLOG_INFO( "        ::Output-from-layers:   {}", output_each_layer );
    SPDLOG_INFO( "        ::Output-from-chart:    {}", output_chart );
    SPDLOG_INFO( "        ::Output-path:          {}", output_path.string() );
    SPDLOG_INFO( "        ::Output-as-kml:        {}", output_as_kml );
    SPDLOG_INFO( "        ::Output-as-png:        {}", output_as_png );
    SPDLOG_INFO( "<<< Finished Loading Arguments." );

    // /\ /\ /\ /\ /\ /\    Load Configuration      /\ /\ /\ /\ /\ /\.
    // ====== ====== ====== ====== ====== ======    ====== ====== ====== ====== ====== ======    ====== ====== ====== ====== ====== ======
    //  \/ \/ \/ \/ \/ \/   Execute Configuration   \/ \/ \/ \/ \/ \/ 

    GDALAllRegister();

    chartbox::ChartBox box;

    { // ====== ====== Load Input Files ====== ======
        auto& mapping = box.mapping();

        SPDLOG_INFO( ">>> Starting Load:");
        const auto start_load_all = std::chrono::high_resolution_clock::now(); 

        // ====== ====== Load Boundary File ====== ======
        if ( ! boundary_input_path.empty() ) {
            auto& layer = box.get_boundary_layer();
            SPDLOG_INFO( ">>> Load Boundary from:     '{}'    to Layer: {}", boundary_input_path.string(), layer.name() );
            if( boundary_input_path.extension() == chartbox::io::geojson::extension ){
                geojson::load_boundary_box( boundary_input_path, mapping );

                layer.sectors_across_view(4);
                layer.meters_across_cell(8.0);
                layer.track( mapping.local_bounds() );

                geojson::load_boundary_layer( boundary_input_path, mapping, layer );
            }
        }

        // ====== ====== Load Contour File ====== ======
        const auto start_load_contour = std::chrono::high_resolution_clock::now(); 
        if ( cache_enable ) {
            auto& to_layer = box.get_contour_layer();

            // if( clear_cache ) {
            //     const size_t files_deleted_count = std::filesystem::remove_all(cache_path);
            //     SPDLOG_INFO(">>> Cleared Cache: {} files\n", files_deleted_count );
            // }
            if ( ! contour_input_path.empty() ) {
                SPDLOG_INFO(">>> Populating cache:        '{}'    <<== '{}'", cache_path.string(), contour_input_path.string() );

                // updates track-bounds and view-bounds
                to_layer.track( mapping.local_bounds() );

                if( contour_input_path.extension() == chartbox::io::geojson::extension ){
                    chartbox::layer::rolling::RollingGridLayer<to_layer.cells_across_sector()> load_window;
                    load_window.enable_cache( cache_path );
                    geojson::process_contour_to_cache( contour_input_path, mapping, load_window );
                }
            }

            SPDLOG_INFO( ">>> Loading from cache:     '{}'    to layer: {}", cache_path.string(), to_layer.name() );
            to_layer.enable_cache( cache_path );
            to_layer.view( LocalLocation(0,0) );
            to_layer.load_from_cache();

            const auto finish_load_contour = std::chrono::high_resolution_clock::now(); 
            const auto load_duration = static_cast<double>(std::chrono::duration_cast<std::chrono::milliseconds>(finish_load_contour - start_load_contour).count())/1000;
            if( 0.5 < load_duration ){
                SPDLOG_INFO( "<< Loaded Contour Layer in:   {:5.2f} s \n", load_duration );
            }
        }

        const auto finish_load_all = std::chrono::high_resolution_clock::now(); 
        const auto load_duration = static_cast<double>(std::chrono::duration_cast<std::chrono::milliseconds>(finish_load_all - start_load_all).count())/1000;
        if( 0.5 < load_duration ){
            SPDLOG_INFO( "<< Loaded all layers in:   {:5.2f} s \n\n", load_duration );
        }
    }

    // ====== ====== Print Debugging on Loaded Chart: ====== ======
    {
        //print the resultant bounds:
        // box.mapping().print();

        // print a summary of layers in the chartbox...
        box.print_layers() ;

        {
            const auto boundary_layer = box.get_boundary_layer();
            SPDLOG_INFO( "====== ====== Layer:{} ====== ====== \n", boundary_layer.name() );
            SPDLOG_INFO( "{}\n", boundary_layer.to_property_string() );
        }{
            const auto contour_layer = box.get_contour_layer();
            SPDLOG_INFO( "====== ====== Layer:{} ====== ====== \n", contour_layer.name() );
            SPDLOG_INFO( "{}\n", contour_layer.to_property_string() );
        }
    }   // DEBUG

    { // ====== ====== Write Output Files ====== ======
        const auto start_write_all = std::chrono::high_resolution_clock::now();

        // ====== ====== Write Boundary Layer to File ====== ======
        if( output_each_layer && output_as_png ){
            const auto& layer = box.get_boundary_layer();
            const auto& bounds = box.mapping().local_bounds();
            const std::filesystem::path boundary_output_path = output_path / "boundary.png";
            SPDLOG_INFO( ">>> Write Layer: {}    to: {}\n", layer.name(), boundary_output_path.string() );
            png::save( layer, bounds, boundary_output_path );
        }

        // ====== ====== Write Contour Layer to File ====== ======
        if( output_each_layer && output_as_png ){
            const auto& layer = box.get_boundary_layer();
            const auto& bounds = box.mapping().local_bounds();
            const std::filesystem::path boundary_output_path = output_path / "contour.png";
            SPDLOG_INFO( ">>> Write Layer: {}    to: {}\n", layer.name(), boundary_output_path.string() );
            png::save( layer, bounds, boundary_output_path );
        }

        // ====== ====== Write Entire Chart to File ====== ======
        if( output_chart && output_as_png ){
            const std::filesystem::path chart_output_path = output_path / "chart.png";
            SPDLOG_INFO( ">>> Write chart to: {}\n", chart_output_path.string() );
            png::save( box, 8.0, chart_output_path );
        }

        const auto finish_write_all = std::chrono::high_resolution_clock::now(); 
        const auto write_duration = static_cast<double>(std::chrono::duration_cast<std::chrono::milliseconds>(finish_write_all - start_write_all).count())/1000;
        if( 0.5 < write_duration ){
            SPDLOG_INFO( "<<< Written in:   {:5.2f} s \n\n", write_duration );
        }
    }

    // make sure this only happens once
    GDALDestroyDriverManager();

    return 0;
}
